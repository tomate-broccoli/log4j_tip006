// sample/Main.java
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main{
    public static void main(String[] args){
        System.out.println("Hello, World!");

        new Main().mainProc();
    }

    public void mainProc(){
        Logger logger = LogManager.getLogger(Main.class);

        logger.info("Hello, Log4j!");
    }
}